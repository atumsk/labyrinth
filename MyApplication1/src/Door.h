#ifndef Door_H
#define Door_H

#include "Entity.h"

class Door : public Entity
{
public:
	Door();
	~Door();

	virtual void init();
	virtual void init(int x, int y);
	virtual void init(int graphic, int x, int y, int w, int h);

	virtual void render();
	virtual void update();

	void updateControls();
	void updateGraphic();

	bool isOfClass(std::string classType);
	std::string getClassName() { return "Door"; };

protected:
	int mpTimer;
};

#endif