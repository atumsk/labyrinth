//Include our classes
#include "Door.h"
#include "singletons.h"


Door::Door() : Entity() {
}

Door::~Door() {
}

void Door::init() {
	Entity::init();
}
void Door::init(int x, int y) {
	Entity::init(x, y);
}
void Door::init(int graphic, int x, int y, int w, int h) {
	Entity::init(graphic, x, y, w, h);
}

void Door::render() {
	Entity::render();
}

void Door::update() {
	Entity::update();
}

void Door::updateControls() {
	return;
}

void Door::updateGraphic() {
}

bool Door::isOfClass(std::string classType) {
	if (classType == "Door" ||
		classType == "Entity") {
		return true;
	}
	return false;
}