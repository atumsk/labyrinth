#ifndef DIRECTOR_H
#define DIRECTOR_H

//Clase Madre
#include "Scene.h"


class SceneDirector
{
public:
	//! Enum Scenes.
	/*! All the Scenes in the game. */
	enum SceneEnum {
		MAIN, LEVEL, GAME_OVER,
		SCENE_FIRST = MAIN, SCENE_LAST = GAME_OVER
	};
	enum SceneFlags {
		FLAG1, FLAG2, FLAG3,
		FLAG_FIRST = FLAG1, FLAG_LAST = FLAG3
	};

	SceneDirector();
	~SceneDirector();

	void init();
	void exitGame();

	void changeScene(SceneEnum next_scene, bool load_on_return = true, bool history = true);
	void setPreviousScene(SceneEnum prev_scene) { mPrevScene = prev_scene; };
	void goBack(bool load_on_return = true);

	SceneEnum getCurrSceneEnum() { return mCurrScene; };
	SceneEnum getPrevSceneEnum() { return mPrevScene; };

	Scene* getCurrentScene();
	Scene* getScene(SceneEnum s) { return mVectorScenes[s]; };

	std::string getClassName() { return "SceneDirector"; };

	//! Gets Singleton instance
	/*!
		\return Instance of ResourceManager (Singleton).
	*/
	static SceneDirector* getInstance();

protected:
	std::vector<Scene*> mVectorScenes;
	std::vector<bool>	mFlags;

	SceneEnum	mCurrScene;
	SceneEnum	mPrevScene;

	static SceneDirector*		pInstance;		/*!<  Singleton instance*/
};

#endif