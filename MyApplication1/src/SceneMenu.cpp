#include "singletons.h"

//Include our classes
#include "SceneMenu.h"

SceneMenu::SceneMenu() : Scene() { //Calls constructor in class Scene
}

SceneMenu::~SceneMenu() {
}

void SceneMenu::init() {
	Scene::init(); //Calls to the init method in class Scene
	mpGraphicID = sResManager->getGraphicID("screen.jpg");
	mpGraphicID2 = sResManager->getGraphicID("icon.png");

	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = sResManager->getGraphicWidth(mpGraphicID);
	mpGraphicRect.h = sResManager->getGraphicHeight(mpGraphicID);

	mpSelection = -1;
	mpText = new ofTrueTypeFont();
	mpText->loadFont("ReturnofGanon.ttf", 36);
}

void SceneMenu::load() {
	Scene::load(); //Calls to the init method in class Scene
}

void SceneMenu::updateScene() {
	inputEvent();
}

void SceneMenu::drawScene() {
	ofSetColor(255, 0, 0);
	imgRender(mpGraphicID, 0, 0, mpGraphicRect, 255);
	imgRender(mpGraphicID2, 700, 400 + mpSelection * 40, mpGraphicRect, 255);

	ofSetColor(218, 165, 32);
	mpText->drawString("The Cave", 400, 480);

	ofSetColor(0, 128, 128);
	mpText->drawString("Continue", 750, 440);
	mpText->drawString("New Game", 750, 480);
	mpText->drawString("Exit Game", 750, 520);
}

//-------------------------------------------
//				   INPUT
//-------------------------------------------
void SceneMenu::inputEvent() {
	//if (key_released[13]) {	// ENTER
	//if (key_released[8]) {	// BACKSPACE
	//if (key_released[9]) {	// TAB
	//if (key_released[127]) {	// SUPR
	if (key_released[' ']) { //SPACE
		if (mpSelection == 1) {
			sDirector->changeScene(SceneDirector::LEVEL);
		}
		if (mpSelection == 0) {

		}
		if (mpSelection == 2) {
			sDirector->exitGame();
		}
	}
	if (key_pressed['s']) {
		mpSelection++;
	}
	if (key_pressed['w']) {
		mpSelection--;
	}
	if (mpSelection < 0) {
		mpSelection = 2;
	}
	if (mpSelection > 2) {
		mpSelection = 0;
	}
}