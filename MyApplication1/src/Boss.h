#ifndef BOSS_H
#define BOSS_H

#include "Entity.h"

class Boss : public Entity
{
public:
	Boss();
	~Boss();

	virtual void init();
	virtual void init(int x, int y);
	virtual void init(int graphic, int x, int y, int w, int h);

	virtual void render();
	virtual void update();

	void updateControls();
	void updateGraphic();

	bool isOfClass(std::string classType);
	std::string getClassName() { return "Boss"; };

protected:
	int mpTimer;
};

#endif