#ifndef SceneGameOver_H
#define SceneGameOver_H

#include "Scene.h"

//! SceneGameOver class
/*!
	Handles the Scene for the main menu of the game.
*/
class SceneGameOver : public Scene
{
public:
	//! Constructor of an empty SceneGameOver.
	SceneGameOver();

	//! Destructor
	~SceneGameOver();

	//! Initializes the Scene.
	virtual void init();

	//! Loads the scene (reinitializes)
	virtual void load();

	//! Returns the ClassName of the object
	/*!
		\return ClassName (as a string)
	*/
	virtual std::string getClassName() { return "SceneGameOver"; };

protected:
	//! Updates the Scene
	void updateScene();

	//! Draws the Scene
	void drawScene();

	//! Takes keyboard input and performs actions
	virtual void inputEvent();

private:
	int			mpGraphicID;

	ofTrueTypeFont* mpText;

	C_Rectangle	mpGraphicRect;
};

#endif