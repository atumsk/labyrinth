//Include our classes
#include "Enemy.h"
#include "singletons.h"


Enemy::Enemy() : Entity() {
	mpSpeed = 200;
	mpTimer = 0;
}

Enemy::~Enemy() {
}

void Enemy::init() {
	Entity::init();
}
void Enemy::init(int x, int y) {
	Entity::init(x, y);
}
void Enemy::init(int graphic, int x, int y, int w, int h) {
	Entity::init(graphic, x, y, w, h);
}

void Enemy::render() {
	Entity::render();
}

void Enemy::update() {
	Entity::update();
}

void Enemy::updateControls() {
	mpMoving = false;
	if (mpTimer == 0) {
		int move_enemy = rand() % 4;
		mpDirection = move_enemy;
		mpTimer = 1000;
	}else{
		mpMoving = true;
		mpTimer -= global_delta_time;
		if (mpTimer < 0) {
			mpTimer = 0;
		}
	}
	return;
}

void Enemy::updateGraphic() {
	if (mpDirection != NONE) {
		mCurrentFrameTime += global_delta_time;
		if (mCurrentFrameTime > 80) {
			mCurrentFrameTime = 0;
			mFrame++;
			if (mFrame >= 3) {
				mFrame = 0;
			}
		}
	}
	else {
		mFrame = 0; 
		mCurrentFrameTime = 0;
	}
	int row = mpDirection - 1;
	if (row < 0) { row = 0; }
	mpGraphicRect.x = mFrame*mpGraphicRect.w;
	mpGraphicRect.y = row*mpGraphicRect.h;
}

bool Enemy::isOfClass(std::string classType) {
	if (classType == "Enemy" ||
		classType == "Entity") {
		return true;
	}
	return false;
}