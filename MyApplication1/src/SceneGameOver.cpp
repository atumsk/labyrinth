#include "singletons.h"

//Include our classes
#include "SceneGameOver.h"

SceneGameOver::SceneGameOver() : Scene() { //Calls constructor in class Scene
}

SceneGameOver::~SceneGameOver() {
}

void SceneGameOver::init() {
	Scene::init(); //Calls to the init method in class Scene
	mpGraphicID = sResManager->getGraphicID("gameover.jpg");

	mpGraphicRect.x = 0;
	mpGraphicRect.y = 0;
	mpGraphicRect.w = sResManager->getGraphicWidth(mpGraphicID);
	mpGraphicRect.h = sResManager->getGraphicHeight(mpGraphicID);

	mpText = new ofTrueTypeFont();
	mpText->loadFont("ReturnofGanon.ttf", 50);
}

void SceneGameOver::load() {
	Scene::load(); //Calls to the init method in class Scene
}

void SceneGameOver::updateScene() {
	inputEvent();
}

void SceneGameOver::drawScene() {
	ofSetColor(255, 0, 0);
	imgRender(mpGraphicID, 0, 0, mpGraphicRect, 255);

	ofSetColor(255, 69, 0);
	mpText->drawString("Press 'R' to restart", 300, 525);
}

//-------------------------------------------
//				   INPUT
//-------------------------------------------
void SceneGameOver::inputEvent() {
	//if (key_released[13]) {	// ENTER
	//if (key_released[8]) {	// BACKSPACE
	//if (key_released[9]) {	// TAB
	//if (key_released[127]) {	// SUPR
	if (key_pressed['r']) {
		sDirector->changeScene(SceneDirector::MAIN);
	}
}