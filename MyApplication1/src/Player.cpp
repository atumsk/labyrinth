//Include our classes
#include "Player.h"
#include "singletons.h"


Player::Player() : Entity() {
	mpSpeed = 250;
}

Player::~Player() {
}

void Player::init() {
	Entity::init();
}
void Player::init(int x, int y) {
	Entity::init(x, y);
}
void Player::init(int graphic, int x, int y, int w, int h) {
	Entity::init(graphic, x, y, w, h);
}

void Player::render() {
	Entity::render();
}

void Player::update() {
	Entity::update();

}

void Player::updateControls() {
	mpMoving = false;
	if (key_down['W'] || key_down['w']) {
		mpDirection = UP;
		mpMoving = true;
	}
	if (key_down['A'] || key_down['a']) {
		mpDirection = LEFT;
		mpMoving = true;
	}
	if (key_down['S'] || key_down['s']) {
		mpDirection = DOWN;
		mpMoving = true;
	}
	if (key_down['D'] || key_down['d']) {
		mpDirection = RIGHT;
		mpMoving = true;
	}
	return;
}

void Player::updateGraphic() {
	if (mpDirection != NONE) {
		mCurrentFrameTime += global_delta_time;
		if (mCurrentFrameTime > 80) {
			mCurrentFrameTime = 0;
			mFrame++;
			if (mFrame >= 3) {
				mFrame = 0;
			}
		}
	}
	else {
		mFrame = 0;
		mCurrentFrameTime = 0;
	}
	int row = mpDirection - 1;
	if (row < 0) { row = 0; }
	mpGraphicRect.x = mFrame*mpGraphicRect.w;
	mpGraphicRect.y = row*mpGraphicRect.h;
}

bool Player::isOfClass(std::string classType) {
	if (classType == "Player" ||
		classType == "Entity") {
		return true;
	}
	return false;
}