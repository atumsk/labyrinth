#include "singletons.h"

//Include our classes
#include "Scene.h"

Scene::Scene() {
	mLoaded = false;
	mReload = true;
}

Scene::~Scene() {
}

void Scene::init() {
	mLoaded = false;
	return;
}

void Scene::load() {
	if (mReload) {
		//Do a thing if re-entering
		//For example, reload a map
	}
	else {
		//Do a thing if not re-entering
	}
	mLoaded = true;
	return;
}

void Scene::onUpdate() {
	updateScene();
	return;
}

void Scene::onDraw() {
	drawScene();
	return;
}

void Scene::inputEvent() {
	return;
}