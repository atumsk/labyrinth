#ifndef ENEMY_H
#define ENEMY_H

#include "Entity.h"

class Enemy : public Entity
{
public:
	Enemy();
	~Enemy();

	virtual void init();
	virtual void init(int x, int y);
	virtual void init(int graphic, int x, int y, int w, int h);

	virtual void render();
	virtual void update();

	void updateControls();
	void updateGraphic();

	bool isOfClass(std::string classType);
	std::string getClassName() { return "Enemy"; };

protected:
	int mpTimer;
};

#endif