#include "singletons.h"
//#include "ResourceManager.h"

#include <algorithm>
#include <sstream>

ResourceManager* ResourceManager::pInstance = NULL;

ResourceManager* ResourceManager::getInstance() {
	//Si se ha inicializado, devuelve la instancia, si no
	//inicializa y la devuelve.
	if (!pInstance) {
		pInstance = new ResourceManager();
	}
	return pInstance;
}

ResourceManager::ResourceManager() {
	mFirstFreeSlot = 0;
}

ResourceManager::~ResourceManager() {
	mGraphicsMap.clear();
	mGraphicsVector.clear();
	mIDMap.clear();
}

int ResourceManager::addGraphic(const char* file) {
	//Carga una imagen y la guarda en los contenedores
	ofImage* graph1 = new ofImage();

	bool loaded = graph1->load(file);

	if (!loaded) {
		std::cerr << "Error when loading image: " << file << std::endl;
		return -1;
	}
	else {
		mGraphicsMap.insert(std::pair<std::string, ofImage*>(file, graph1));
		int returnValue = 0;
		if (mFirstFreeSlot == mGraphicsVector.size()) {
			mGraphicsVector.push_back(graph1);
			mFirstFreeSlot++;
			returnValue = mGraphicsVector.size() - 1;
		}
		else {
			mGraphicsVector[mFirstFreeSlot] = graph1;
			int temp = mFirstFreeSlot;
			mFirstFreeSlot = updateFirstFreeSlot();
			returnValue = temp;
		}
		return returnValue;
	}
}

ofImage* ResourceManager::getGraphic(const char* file) {
	// Si la imagen ha sido cargada, devuelve el puntero
	// Si no, carga la imagen y devuelve el puntero

	std::map<std::string, ofImage*>::iterator it = mGraphicsMap.find(file);
	if (it == mGraphicsMap.end()) {
		std::cout << "Adding Graphic " << file << " (first request)" << std::endl;
		addGraphic(file);
		it = mGraphicsMap.find(file);
	}
	return it->second;
}

int ResourceManager::getGraphicID(const char* file) {
	// Mira si el gr�fico ya ha sido cargado
	// Si no ha sido cargado, lo carga y devuelve la �ltima posici�n del vector
	// Si ya est� en el map, busca el gr�fico en el vector y devuelve la posici�n

	std::map<std::string, ofImage*>::iterator it = mGraphicsMap.find(file);
	int returnValue = -1;
	if (it == mGraphicsMap.end()) {
		std::cout << "Adding Graphic " << file << " (first request)" << std::endl;
		int index = addGraphic(file);
		mIDMap.insert(std::pair<std::string, int>(file, index));
		returnValue = index;
	}
	else {
		std::map<std::string, int>::iterator iter = mIDMap.find(file);
		if (iter == mIDMap.end()) {
			std::cout << "Failed to find " << file << " on the ID map" << std::endl;
			returnValue = searchGraphic(it->second);
		}
		else {
			returnValue = iter->second;
		}
	}

	return returnValue;
}

ofImage* ResourceManager::getGraphicByID(int ID) {
	// Si el ID que se le pide est� dentro del rango del vector,
	// devuelve el valor en esa posici�n
	// Una ID nunca sera <0 porque size es sin signo. Aqui GCC daba un warning.
	if (ID < mGraphicsVector.size()) {
		return mGraphicsVector[ID];
	}
	else {
		return NULL;
	}
}

int ResourceManager::searchGraphic(ofImage* img) {
	// Busca el gr�fico en el vector, si no lo encuentra, devuelve -1

	for (int i = 0; i < mGraphicsVector.size(); i++) {
		if (mGraphicsVector[i] == img) {
			return i;
		}
	}
	return -1;
}

std::string ResourceManager::getGraphicPathByID(int ID) {
	if (ID >= mGraphicsVector.size()) { return "NULL"; }
	ofImage* aSurface = mGraphicsVector[ID];

	std::map<std::string, ofImage*>::iterator it;
	for (it = mGraphicsMap.begin(); it != mGraphicsMap.end(); ++it) {
		if (it->second == aSurface) {
			return it->first;
		}
	}
	return "NULL";
}


int ResourceManager::updateFirstFreeSlot() {
	for (int i = 0; i < mGraphicsVector.size(); i++) {
		if (mGraphicsVector[i] == NULL) {
			return i;
		}
	}
	return mGraphicsVector.size();
}

void ResourceManager::printLoadedGraphics() {
	std::map<std::string, ofImage*>::iterator it = mGraphicsMap.begin();
	std::cout << "---- Loaded Graphics ----" << std::endl;
	while (it != mGraphicsMap.end()) {
		std::cout << it->first << std::endl;
		it++;
	}
	std::cout << "-------------------------" << std::endl;
}


void ResourceManager::getGraphicSize(int img, int &width, int &height) {
	width = 0;
	height = 0;
	if (img != -1) {
		width = getGraphicByID(img)->getWidth();
		height = getGraphicByID(img)->getHeight();
	}
}

int ResourceManager::getGraphicWidth(int img) {
	if (img != -1) {
		return getGraphicByID(img)->getWidth();
	}
	return 0;
}

int ResourceManager::getGraphicHeight(int img) {
	if (img != -1) {
		return getGraphicByID(img)->getHeight();
	}
	return 0;
}